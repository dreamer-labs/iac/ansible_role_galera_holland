Role Name
=========

This role is used to deploy out the holland backup service for a galara cluster using xtrabackup

Requirements
------------
None

Role Variables
--------------
There is a list of vars that can be set in the `defaults/main`

```yaml
# mysql client connection info -- uncomment and add if needed
# galera_holland_mysql_user:
# galera_holland_mysql_password:
# galera_holland_mysql_host:
# galera_holland_mysql_port:
# galera_holland_mysql_socket:
```




Dependencies
------------

Referenece requirements.yml


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: galera
      become: true
      gather_facts: true
      roles:
         - role: ansible_role_galera_holland


License
-------

MIT

Author Information
------------------

Global InfoTek, Inc
